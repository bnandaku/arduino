#include "FastLED.h"
#include "SPI.h"
#define NUM_LEDS 48
#define NUM_ROWS 48
#define DATA_PIN 2
#define CLOCK_PIN 3
#define HWSERIAL Serial1
const int LED = 13;
CRGB leds[NUM_LEDS];
byte byteArray[5];
byte image[NUM_ROWS][NUM_LEDS][3];
void setup(){
Serial.begin(9600);
HWSERIAL.begin(115200);
pinMode(LED,OUTPUT);
FastLED.addLeds<LPD8806, DATA_PIN, CLOCK_PIN, RGB>(leds, NUM_LEDS);
FastLED.show();
}

void loop(){
if(HWSERIAL.peek() != -1){
  Serial.println(HWSERIAL.peek());
digitalWrite(LED,HIGH);
  receiveImage();
  
  digitalWrite(LED,LOW);
}


else{
  Serial.println("Display Started");
  displayImage();
  Serial.println("Display Ended");
  }
}


void receiveImage(){


for(int i = 0; i<5;i++){
  if(HWSERIAL.available()){
    
    
    byte incomingByte = HWSERIAL.read();
    Serial.println(incomingByte);
    if(incomingByte == -1){
    loop();
    }
    byteArray[i] = incomingByte;
    if(i == 0){
      if(byteArray[i] >NUM_ROWS){
      i--;
      }
    }
    else if(i ==1){
        if(byteArray[i] >NUM_LEDS){
      i--;
      }
    }
    
  if(i ==4){
  processByte(byteArray);
  }
 
}
  
else {i--;}
}


}

void processByte(byte byteArray1[]){
  image[byteArray1[0]][byteArray1[1]][0]= byteArray1[2];
  image[byteArray1[0]][byteArray1[1]][1]= byteArray1[3];
  image[byteArray1[0]][byteArray1[1]][2]= byteArray1[4];    
}

void displayImage(){
    for(int i = 0; i < NUM_ROWS;i++){
      
      for(int j = 0; j < NUM_LEDS;j++){
        leds[j].setRGB(image[i][j][0],image[i][j][1],image[i][j][2]);
//        Serial.print("Row: ");
//        Serial.print(i);
//         Serial.print(" LED: ");
//        Serial.print(j);
//         Serial.print(" RED: ");
//        Serial.print(image[i][j][0]);
//          Serial.print(" GREEN: ");
//        Serial.print(image[i][j][0]);
//          Serial.print(" BLUE: ");
//        Serial.print(image[i][j][0]);
//        Serial.println("\n");
      }
     FastLED.show();
    }
}
