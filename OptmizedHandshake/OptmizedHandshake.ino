//Best Code I have got right now.. 
//This works way too efficiently and makes my life easier. 
// The LED's are too slow for this though 


#include "FastLED.h"
#include "SPI.h"
#define NUM_LEDS 48
#define DATA_PIN 3
#define CLOCK_PIN 4
#define HWSERIAL Serial1
#define numBytes 64
const int LED = 13;
CRGB leds[NUM_LEDS];
int byteArray[numBytes];
void setup(){
Serial.begin(115200);
HWSERIAL.begin(115200);
pinMode(LED,OUTPUT);
FastLED.addLeds<WS2801, DATA_PIN, CLOCK_PIN, RGB>(leds, NUM_LEDS);
FastLED.show();
}



void loop(){
//Since the Serial Buffer is only 64bytes long
// I made the code read 64 bytes of data and push
// There is an error rate of 2.7%
// We need 4 bytes per LED.. Pixel, Red, Blue and Green
//I have to keep in mind the error rate of 2.74%
  for(int i = 0; i < numBytes;i++){
    if(HWSERIAL.available()){
      digitalWrite(LED, HIGH);

    int incomingByte = HWSERIAL.read();

    byteArray[i] = incomingByte;
                Serial.print(incomingByte);
                Serial.print("-");
    //Error Correction 
    // If there is an error all 4 bytes are removed
          if((i%4) == 0){
            Serial.print("\n");
            if(byteArray[i] > NUM_LEDS){ 
            i--;
            }
      }
    digitalWrite(LED,LOW);
     
    }
    
    else{
      i--;
    }
    
    if(i == numBytes-1){
    
      processBytes(byteArray);
    }
  
    }

}

void processBytes( int byteArray1[]){
    
  for(int i = 0; i < numBytes/4;i++){
    
    leds[byteArray1[i]].setRGB(byteArray[i+1],byteArray[i+2],byteArray[i+3]);
  }
  // Displays 16 pixels at a time
  FastLED.show();
  delay(10);
}
