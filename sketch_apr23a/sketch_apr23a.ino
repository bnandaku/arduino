//Best Code I have got right now.. 
//This works way too efficiently and makes my life easier. 
// The LED's are too slow for this though 
// Optmized handshake but also stores the image data 
// images will be 48 by 48 
// this will take approx 9KB of RAM
// Hopefully the Teensy will be good for this or I will use something else
// Teensy 3 has 16 KB ram and teensy 3.1 has 64KB


#include "FastLED.h"
#include "SPI.h"
#define NUM_LEDS 48
#define NUM_ROWS 48
#define DATA_PIN 3
#define CLOCK_PIN 4
#define HWSERIAL Serial1
#define numBytes NUM_LEDS*4
const int LED = 13;
int currentRow =0;
CRGB leds[NUM_LEDS];
byte imageArray[NUM_ROWS][NUM_LEDS][4];
byte byteArray[numBytes];
void setup(){
Serial.begin(115200);
HWSERIAL.begin(115200);
pinMode(LED,OUTPUT);
FastLED.addLeds<WS2801, DATA_PIN, CLOCK_PIN, RGB>(leds, NUM_LEDS);
FastLED.show();
}



void loop(){
//Since the Serial Buffer is only 64bytes long
// I made the code read 192 bytes of data and push
// There is an error rate of 2.7%
// We need 4 bytes per LED.. Pixel, Red, Blue and Green
//I have to keep in mind the error rate of 2.74%
 if(HWSERIAL.available()){
  for(int i = 0; i < numBytes;i++){
    if(HWSERIAL.available()){
      digitalWrite(LED, HIGH);

    int incomingByte = HWSERIAL.read();

    byteArray[i] = incomingByte;
                Serial.print(incomingByte);
                Serial.print("-");
    //Error Correction 
    // If there is an error all 4 bytes are removed
          if((i%4) == 0){
            Serial.print("\n");
            if(byteArray[i] > NUM_LEDS){ 
            i--;
            }
      }
    digitalWrite(LED,LOW);
     
    }
    
    else{
      i--;
    }
    
    if(i == numBytes-1){
    
      processBytes(byteArray , currentRow);
      if(currentRow < NUM_ROWS)
      currentRow++;
      else 
      currentRow = 0;
    }
  }  
 
}


}

void processBytes(byte byteArray1[],int currentRow){
    
  for(int i = 0; i<numBytes/4;i++){
    imageArray[currentRow][i][0] = byteArray[i];
    imageArray[currentRow][i][1] = byteArray[i+1];
    imageArray[currentRow][i][2] = byteArray[i+2];
    imageArray[currentRow][i][3] = byteArray[i+3];
    
  }
  // Displays 16 pixels at a time
  FastLED.show();
  delay(10);
}

void displayImg(){

  for(int i = 0; i<48;i++){
  
    for(int j =0; j<48;j++){
    Serial.print("Row: ");
    Serial.print(i);
    Serial.print("Pixel: ");
    Serial.print(imageArray[i][j][0]);
        Serial.print("Red: ");
    Serial.print(imageArray[i][j][1]);
        Serial.print("Green: ");
    Serial.print(imageArray[i][j][2]);
         Serial.print("Blue: ");
    Serial.print(imageArray[i][j][3]);
    Serialprintln("");
    
   //   leds[imageArray[i][j][0]].setRGB(imageArray[i][j][1],imageArray[i][j][2],imageArray[i][j][3]);
    }
  FastLED.show();
  }
}
