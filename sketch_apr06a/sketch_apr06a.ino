const int ShiftPWM_latchPin=8;
const int ShiftPWM_dataPin = 11;
const int ShiftPWM_clockPin = 13;
const bool ShiftPWM_invertOutputs = false;
const bool ShiftPWM_balanceLoad = true;

#include <ShiftPWM.h>  
unsigned char maxBrightness = 150;
unsigned char pwmFrequency = 74;
int numRegisters = 15;
int numOutputs = numRegisters*8;
int numRGBLeds = numRegisters*8/3;
int fadingMode = 0; 
const int button = 3;
int buttonVal = 0;
int state = 0;
unsigned long startTime = 0; 

void setup()   {                
  Serial.begin(9600);
  ShiftPWM.SetAmountOfRegisters(numRegisters);
  ShiftPWM.SetPinGrouping(1); 
  ShiftPWM.Start(pwmFrequency,maxBrightness);
  pinMode(button, INPUT);

}
int count=0;
void loop(){
  if(state > 0){
button = digitalRead(button);
if(button == High){
  
  rgbLedRainbow(3000,numRGBLeds);
  state = 1 - state;
}

}

else{
//if(Serial.available()){
//int pixel = Serial.parseInt();
//int red = Serial.parseInt();
//int green = Serial.parseInt();
//int blue = Serial.parseInt();
//setRGB(pixel,red,green,blue);
//delay(10);
//}
  state = 0;

}

}

void setRGB(int pixel, int r, int g,int b){
ShiftPWM.SetRGB(pixel,r,g,b);
}

void rgbLedRainbow(unsigned long cycleTime, int rainbowWidth){
  // Displays a rainbow spread over a few LED's (numRGBLeds), which shifts in hue. 
  // The rainbow can be wider then the real number of LED's.
  unsigned long time = millis()-startTime;
  unsigned long colorShift = (360*time/cycleTime)%360; // this color shift is like the hue slider in Photoshop.

  for(int led=0;led<numRGBLeds;led++){ // loop over all LED's
    int hue = ((led)*360/(rainbowWidth-1)+colorShift)%360; // Set hue from 0 to 360 from first to last led and shift the hue
    ShiftPWM.SetHSV(led, hue, 255, 255); // write the HSV values, with saturation and value at maximum
  }
}
