#include<SD.h>

const int ShiftPWM_latchPin=8;
const int ShiftPWM_dataPin = 11;
const int ShiftPWM_clockPin = 13;
const bool ShiftPWM_invertOutputs = false;
const bool ShiftPWM_balanceLoad = false;

#include <ShiftPWM.h>  
unsigned char maxBrightness = 180;
unsigned char pwmFrequency = 74;
int numRegisters = 15;
int numOutputs = numRegisters*8;
int numRGBLeds = numRegisters*8/3;
int fadingMode = 0; 
File myFile;
unsigned long startTime = 0; 

void setup()   {                
  Serial.begin(9600);
  ShiftPWM.SetAmountOfRegisters(numRegisters);
  ShiftPWM.SetPinGrouping(1); 
  ShiftPWM.Start(pwmFrequency,maxBrightness);
  //Serial.println("Initializing SD card...");
  // On the Ethernet Shield, CS is pin 4. It's set as an output by default.
  // Note that even if it's not used as the CS pin, the hardware SS pin 
  // (10 on most Arduino boards, 53 on the Mega) must be left as an output 
  // or the SD library functions will not work. 
   pinMode(10, OUTPUT);
   digitalWrite(10, LOW);
  if (!SD.begin(10)) {
    Serial.println("initialization failed!");
    return;
  }
  Serial.println("initialization done.");
  

}

void loop(){
  Serial.println("Hi");
 myFile = SD.open("input.txt");
  if (myFile) {
    Serial.println("input.txt:");
    
      // read from the file until there's nothing else in it:
    while (myFile.available()) {
        Serial.write(myFile.read());
    }
    // close the file:
    myFile.close();
  } else {
    // if the file didn't open, print an error:
    Serial.println("error opening test.txt");
  }
  int pixel = Serial.parseInt();
  Serial.print(pixel);
  int r = Serial.parseInt();
  Serial.print(r);
  int g = Serial.parseInt();
  Serial.print(g);
  int b = Serial.parseInt();
  Serial.println(b);
  
  ShiftPWM.SetRGB(pixel,r,g,b);

}
