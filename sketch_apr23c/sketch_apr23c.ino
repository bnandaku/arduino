#include "SPI.h"
#include "Adafruit_WS2801.h"
#define NUM_LEDS 48
#define NUM_ROWS 48
#define DATA_PIN 3
#define CLOCK_PIN 4
#define HWSERIAL Serial1
Adafruit_WS2801 strip = Adafruit_WS2801(NUM_LEDS, DATA_PIN, CLOCK_PIN);
const int LED = 13;
int breaker = 0;
byte byteArray[5];
byte image[NUM_ROWS+1][NUM_LEDS+1][3];
void setup(){
  strip.begin();
  strip.show();
Serial.begin(9600);
HWSERIAL.begin(4800);
pinMode(LED,OUTPUT);
digitalWrite(LED,HIGH);
delay(1000);
digitalWrite(LED,LOW);
}

void loop(){
    if(HWSERIAL.available()){
     digitalWrite(LED,HIGH);
      for(int i = 0; i<5;i++){
         
          byte incomingByte = HWSERIAL.read();
          byteArray[i] = incomingByte;

          if(i ==0 && byteArray[i] > NUM_ROWS ){
            i--;
            Serial.println("Error");
            //breaker++;
          }
          
          else if(i==1 && byteArray[i] > NUM_LEDS){
          i--;
           Serial.println("Error");
          //breaker++;
          
          }
          
          Serial.print(byteArray[i]);
          Serial.print("-");
          if(i ==4){
          processBytes(byteArray);
          
          }
          
          //delay(50);
          //breaker++;
          // digitalWrite(LED,LOW);
      }
      //delay(50);
      digitalWrite(LED,LOW);
      Serial.println("");
      breaker =0;
      
     
    }
    
    else{
        Serial.println("Display Started");
  displayImage();
  Serial.println("Display Ended");
    //Image Display Stuff goes here
    
    }

}

void displayImage(){
    for(int i = 0; i < NUM_ROWS;i++){
      
      for(int j = 0; j < NUM_LEDS;j++){
        strip.setPixelColor(j, Color(image[i][j][0],image[i][j][1],image[i][j][2]));
//        Serial.print("Row: ");
//        Serial.print(i);
//         Serial.print(" LED: ");
//        Serial.print(j);
//         Serial.print(" RED: ");
//        Serial.print(image[i][j][0]);
//          Serial.print(" GREEN: ");
//        Serial.print(image[i][j][0]);
//          Serial.print(" BLUE: ");
//        Serial.print(image[i][j][0]);
//        Serial.println("\n");
//        strip.show();
      }
     strip.show();
 // delay(10);
    }
}
void processBytes(byte byteArray1[]){
  
  if(byteArray1[0]>NUM_ROWS && byteArray1[1] >NUM_LEDS){
      Serial.println("This was Called");
    byteArray1[0] = 0; 
    byteArray1[1] =0;
  }
//Serial.println("ProcessBytes was called");
  image[byteArray1[0]][byteArray1[1]][0] =byteArray1[2];
  image[byteArray1[0]][byteArray1[1]][1] =byteArray1[3];
  image[byteArray1[0]][byteArray1[1]][2] =byteArray1[4];
 // displayImage();
 delay(20);
}

uint32_t Color(byte r, byte g, byte b)
{
  uint32_t c;
  c = r;
  c <<= 8;
  c |= g;
  c <<= 8;
  c |= b;
  return c;
}
